package com.uttam.readphonecontact;


import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;

import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;

import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;


import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private String id, name, phone, image_uri;
    private byte[] contactImage = null;
    private Bitmap bitmap;

    private List<ContactItem> contactItems;

    private AdapterContact adapter;
    private ProgressBar progressBar;
    private GridView gridView;

    //click
    private final Handler mHandler = new Handler();
    private int clicks;
    private boolean isBusy = false;
    TextToSpeech textToSpeech;

    private final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView =  findViewById(R.id.listViewContact);
        progressBar = findViewById(R.id.progressBar);

        textToSpeech=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(Locale.UK);
                }
            }
        });

        checkPermissionAndLoadContacts();
    }

    private void checkPermissionAndLoadContacts(){
        if (ContextCompat.checkSelfPermission(MainActivity.this,  Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.READ_CONTACTS)) {
                Toast.makeText(getApplicationContext(), "Contact Read permission denied! Please grant Contact Read permission from seting to access phone book", Toast.LENGTH_LONG).show();
                Log.d("PHONEBOOK_SAMPLE", "Contact Read permission denied! Please grant Contact Read permission from seting to access phone book");
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }else{
            new ContactInfo().execute();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new ContactInfo().execute();
                } else {
                    Toast.makeText(getApplicationContext(), "Contact Read permission not granted yet! Please grant Contact Read permission to access phone book", Toast.LENGTH_LONG).show();
                    Log.d("PHONEBOOK_SAMPLE", "Contact Read permission not granted yet! Please grant Contact Read permission to access phone book");
                }
                return;
            }
        }
    }

    private void readContacts() {
        contactItems = new ArrayList<>();
        ContentResolver cr =  getApplicationContext().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
                null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                ContactItem item = new ContactItem();
                id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                name = Character.toUpperCase(name.charAt(0)) + name.substring(1);
                image_uri = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id},
                            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC");
                    while (pCur.moveToNext()) {
                        phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        phone = phone.replaceAll("\\s+", "");
                        phone = phone.replaceAll("[^0-9]", "");
                    }
                    pCur.close();
                }
                if (image_uri != null) {
                    try {
                        bitmap = MediaStore.Images.Media
                                .getBitmap(getApplicationContext().getContentResolver(),
                                        Uri.parse(image_uri));
                        contactImage = getImageBytes(bitmap);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    contactImage = null;
                }
                item.setId(id);
                item.setName(name);
                item.setContactImage(contactImage);
                item.setPhone(phone);
                contactItems.add(item);
            }
        }

        Collections.sort(contactItems, new SortByName());
    }

    private byte[] getImageBytes(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        return outputStream.toByteArray();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            default:
                break;
        }
    }

    public void onSingleClick(int position) {
        String wordToSpeak = contactItems.get(position).getName();
        if(wordToSpeak == null || wordToSpeak == ""){
            wordToSpeak = "Content not available";
        }
        textToSpeech.speak(wordToSpeak, TextToSpeech.QUEUE_FLUSH, null);
    }

    public void onDoubleClick(int position) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+contactItems.get(position).getPhone()));

        if (ActivityCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }

    @Override
    public void onPause(){
        if(textToSpeech !=null){
            textToSpeech.stop();
            textToSpeech.shutdown();
        }
        super.onPause();
    }

    private void onGridItemClick(final int position){
        if (!isBusy) {
            isBusy = true;
            clicks++;
            mHandler.postDelayed(new Runnable() {
                public final void run() {
                    if (clicks == 2) {  // Double tap.
                        onDoubleClick(position);
                    }

                    if (clicks == 1) {  // Single tap
                        onSingleClick(position);
                    }
                    if (clicks >= 3) {  // more than 2 tap
//                        onSingleClick(position);
                    }

                    // we need to  restore clicks count
                    clicks = 0;
                }
            }, 500);
            isBusy = false;
        }
    }


    public class ContactInfo extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.GONE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            readContacts();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.GONE);
            gridView.setVisibility(View.VISIBLE);
            setListAdapter();
        }
    }

    private void setListAdapter() {
        adapter = new AdapterContact(getApplicationContext(), contactItems);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onGridItemClick(position);
            }
        });
    }

    class SortByName implements Comparator<ContactItem> {
        @Override
        public int compare(ContactItem o1, ContactItem o2) {
            return o1.name.compareTo( o2.name);
        }
    }
}
